import 'package:apl_project/common_widgets/container.dart';
import 'package:apl_project/utils/image_res.dart';
import 'package:apl_project/utils/string_res.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class CreatNewGroup extends StatefulWidget {
  CreatNewGroup({super.key});

  List<Widget> widgetOptions = <Widget>[
    const Text('Home Page',
        style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
    const Text('Search Page',
        style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
    const Text('Profile Page',
        style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
  ];

  @override
  State<CreatNewGroup> createState() => _CreatNewGroupState();
}

class _CreatNewGroupState extends State<CreatNewGroup> {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    int selectedIndex = 0;
    void onItemTapped(int index) {
      setState(() {
        selectedIndex = index;
      });
    }

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: Padding(
            padding: EdgeInsets.only(top: height * 0.02, left: width * 0.05),
            child: Image.asset(ImageRes.accountCircle),
          ),
          centerTitle: true,
          title: Padding(
            padding: EdgeInsets.only(
              top: height * 0.02,
            ),
            child: const Text(
              StringRes.apl,
              style: TextStyle(fontFamily: "aplFonts", fontSize: 30),
            ),
          ),
          actions: [
            Padding(
              padding: EdgeInsets.only(
                top: height * 0.02,
                left: width * 0.02,
              ),
              child: Image.asset(ImageRes.notificationBell),
            ),
            Padding(
              padding: EdgeInsets.only(
                  top: height * 0.02, left: width * 0.04, right: width * 0.05),
              child: Image.asset(ImageRes.walletIcon),
            ),
          ],
          bottom: const PreferredSize(
              preferredSize: Size(0, 10), child: SizedBox()),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding:
                    EdgeInsets.only(top: height * .0150, right: width * 0.330),
                child: const Text(StringRes.creatNewGroup,
                    style: TextStyle(
                      fontFamily: 'aplFonts',
                      color: Color(0xff0059b4),
                      fontSize: 18,
                      fontWeight: FontWeight.w800,
                    )),
              ),
              SizedBox(height: height * 0.005),
              const Divider(
                  color: Colors.grey, indent: 20, endIndent: 20, thickness: 2),
              SizedBox(height: height * 0.02),
              creatGroupContainer(context),
              SizedBox(height: height * 0.015),
              creatGroupContainer(context),
              SizedBox(height: height * 0.015),
              creatGroupContainer(context),
              SizedBox(height: height * 0.015),
              creatGroupContainer(context),
            ],
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                  icon: SizedBox(
                    height: 40,
                    width: 40,
                    child: Image(
                      height: 40,
                      width: 40,
                      image: AssetImage(
                        ImageRes.home,
                      ),
                    ),
                  ),
                  label: 'Home',
                  backgroundColor: Colors.black),
              BottomNavigationBarItem(
                  icon: SizedBox(
                      height: 40,
                      width: 40,
                      child: Image(image: AssetImage(ImageRes.reward))),
                  label: 'Search',
                  backgroundColor: Colors.yellow),
              BottomNavigationBarItem(
                icon: SizedBox(
                    height: 40,
                    width: 40,
                    child: Image(image: AssetImage(ImageRes.myMatches))),
                label: 'Profile',
                backgroundColor: Colors.blue,
              ),
              BottomNavigationBarItem(
                icon: SizedBox(
                    height: 40,
                    width: 40,
                    child: Image(image: AssetImage(ImageRes.chat))),
                label: 'Profile',
                backgroundColor: Colors.blue,
              ),
              BottomNavigationBarItem(
                icon: SizedBox(
                    height: 40,
                    width: 40,
                    child: Image(image: AssetImage(ImageRes.winner))),
                label: 'Profile',
                backgroundColor: Colors.blue,
              ),
            ],
            type: BottomNavigationBarType.shifting,
            currentIndex: selectedIndex,
            selectedItemColor: Colors.white,
            iconSize: 25,
            onTap: onItemTapped,
            elevation: 5),
      ),
    );
  }
}
