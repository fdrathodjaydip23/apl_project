import 'package:apl_project/common_widgets/container.dart';
import 'package:apl_project/utils/image_res.dart';
import 'package:apl_project/utils/string_res.dart';
import 'package:flutter/material.dart';

class ActionHubScreen extends StatefulWidget {
  const ActionHubScreen({super.key});

  @override
  State<ActionHubScreen> createState() => _ActionHubScreenState();
}

class _ActionHubScreenState extends State<ActionHubScreen> {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      body: SafeArea(
        child: SizedBox(
          height: double.infinity,
          width: double.infinity,
          child: Stack(
            children: [
              Image.asset(
                ImageRes.backGround,
                fit: BoxFit.fill,
              ),
              Center(
                child: BackdropFilter(
                  filter:
                      const ColorFilter.mode(Colors.black87, BlendMode.darken),
                  child: Container(
                    height: height * 0.880,
                    width: width * 0.90,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(25)),
                  ),
                ),
              ),
              Positioned(
                right: width * 0.0850,
                top: height * 0.0630,
                child: Image.asset(ImageRes.closeBack, scale: 0.8),
              ),
              Positioned(
                top: height * 0.09,
                left: width * 0.280,
                child: Column(
                  children: [
                    const Text(
                      StringRes.actionHub,
                      style:
                          TextStyle(fontWeight: FontWeight.w800, fontSize: 19),
                    ),
                    const SizedBox(height: 10),
                    Container(
                      height: 1,
                      width: 200,
                      color: Colors.black,
                    ),
                  ],
                ),
              ),
              Positioned(
                  top: height * 0.16,
                  left: width * 0.062,
                  child: commonContainer3(context)),
              Positioned(
                  top: height * 0.312,
                  left: width * 0.062,
                  child: commonContainer3(context)),
              Positioned(
                  top: height * 0.465,
                  left: width * 0.062,
                  child: commonContainer3(context)),
              Positioned(
                  top: height * 0.6175,
                  left: width * 0.062,
                  child: commonContainer3(context)),
              Positioned(
                  top: height * 0.770,
                  left: width * 0.062,
                  child: commonContainer3(context)),
              Positioned(
                top: height * 0.143,
                left: width * 0.06,
                child: Image.asset(ImageRes.playerImage, scale: 0.990),
              ),
              Positioned(
                top: height * 0.2945,
                left: width * 0.06,
                child: Image.asset(ImageRes.playerImage, scale: 0.990),
              ),
              Positioned(
                top: height * 0.4475,
                left: width * 0.06,
                child: Image.asset(ImageRes.playerImage, scale: 0.990),
              ),
              Positioned(
                top: height * 0.600,
                left: width * 0.06,
                child: Image.asset(ImageRes.playerImage, scale: 0.990),
              ),
              Positioned(
                top: height * 0.753,
                left: width * 0.06,
                child: Image.asset(ImageRes.playerImage, scale: 0.990),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
