import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../utils/image_res.dart';
import '../utils/string_res.dart';

Widget container(BuildContext context) {
  double height = MediaQuery.of(context).size.height;
  double width = MediaQuery.of(context).size.width;
  return Container(
    height: height * 0.085,
    width: width * 0.825,
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      gradient: const LinearGradient(
          colors: [Color(0xff0059b4), Color(0xff65b2ff)],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter),
    ),
    child:
        const Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
      SizedBox(width: 10),
      Column(
        children: [
          SizedBox(
            height: 15,
          ),
          Text(
            StringRes.current,
            style: TextStyle(
              color: Colors.white,
              fontSize: 16.5,
              fontWeight: FontWeight.w500,
            ),
          ),
          Text(
            StringRes.bid,
            style: TextStyle(
                color: Colors.white,
                fontSize: 16.5,
                fontWeight: FontWeight.w500),
          ),
        ],
      ),
      VerticalDivider(
          color: Colors.white, thickness: 1, endIndent: 2, indent: 2.5),
      Text("\$125K",
          style: TextStyle(
            color: Colors.white,
            fontFamily: 'aplFonts',
            fontSize: 38,
            fontWeight: FontWeight.w800,
          )),
      SizedBox(width: 3),
    ]),
  );
}

///-------------------------------------------------------------  Common Container ----------------------------------------------------------------------------///
Widget commonContainer(BuildContext context) {
  double height = MediaQuery.of(context).size.height;
  double width = MediaQuery.of(context).size.width;
  return Container(
    height: height * 0.130,
    width: width * 0.930,
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      gradient: const LinearGradient(
          colors: [Color(0xff0059b4), Color(0xff65b2ff)],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter),
    ),
    child: Stack(
      children: [
        Positioned(
          right: -5,
          child: SizedBox(
              height: 110,
              width: 90,
              child: Image.asset(
                ImageRes.lion,
                fit: BoxFit.fill,
              )),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 80, top: 25),
          child: Column(
            children: [
              const Text(
                StringRes.shikharDhaWan,
                style: TextStyle(
                    fontFamily: 'aplFonts', fontSize: 18, color: Colors.white),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 65),
                child: Row(
                  children: [
                    const Text(StringRes.basePrice,
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                            fontSize: 14)),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(7),
                        color: Colors.white,
                      ),
                      height: 27,
                      width: 60,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 14),
                        child: Text(
                          StringRes.text1cr,
                          style: TextStyle(
                              fontFamily: 'aplFonts',
                              fontSize: 18,
                              color: Colors.blue.shade600),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    ),
  );
}

///-------------------------------------------------------------------- container ---------------------------------------------------------------------------///
Widget commonContainer2(double width, double height, List<Color> color,
    String text, double fontSize,
    {required double top, required double left}) {
  return Container(
    height: height,
    width: width,
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(2),
      gradient: LinearGradient(
          colors: color,
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter),
    ),
    child: Center(
        child: Padding(
      padding: EdgeInsets.only(top: top, left: left),
      child: Text(text,
          style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w500,
              fontSize: fontSize)),
    )),
  );
}

///--------------------------------------------------------------------------- CommonContainer ------------------------------------------------------------------///
Widget commonContainer3(BuildContext context) {
  double height = MediaQuery.of(context).size.height;
  double width = MediaQuery.of(context).size.width;

  return Container(
    height: height * 0.124,
    width: width * 0.875,
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      gradient: const LinearGradient(
          colors: [Color(0xff0059b4), Color(0xff65b2ff)],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter),
    ),
    child: Stack(
      children: [
        Positioned(
          right: -5,
          child: SizedBox(
            height: 110,
            width: 90,
            child: Image.asset(
              ImageRes.lion,
              fit: BoxFit.fill,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(left: width * 0.267, top: height * 0.0180),
          child: Column(
            children: [
              Row(
                children: [
                  const Text(
                    StringRes.shikharDhaWan,
                    style: TextStyle(
                        fontSize: 19,
                        color: Colors.white,
                        fontWeight: FontWeight.w500),
                  ),
                  const SizedBox(width: 9),
                  Container(
                    width: 30,
                    height: 30,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(5),
                    ),
                    child: const Icon(Icons.sports_cricket, color: Colors.blue),
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 4),
                child: Row(
                  children: [
                    Container(
                      width: 48,
                      height: 27.5,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: const Center(
                          child: Text(StringRes.text22by8,
                              style: TextStyle(
                                  color: Colors.black87,
                                  fontWeight: FontWeight.w700))),
                    ),
                    const SizedBox(width: 5),
                    Container(
                      width: 48,
                      height: 27.5,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: const Center(
                        child: Text(
                          StringRes.text250,
                          style: TextStyle(
                              color: Colors.black87,
                              fontWeight: FontWeight.w700),
                        ),
                      ),
                    ),
                    const SizedBox(width: 5),
                    Container(
                      width: 122,
                      height: 28,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: const Center(
                        child: Text(
                          StringRes.allRounderBatter,
                          style: TextStyle(
                              fontSize: 9.3,
                              color: Colors.black87,
                              fontWeight: FontWeight.w900),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(top: 5),
                child: Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 13),
                      child: Text(
                        StringRes.sr,
                        style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 20),
                      child: Text(
                        StringRes.matches,
                        style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 17),
                      child: Text(
                        StringRes.specialization,
                        style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    ),
  );
}

///------------------------------------------------------------------------------- creat New Group __________________________________________________________///
Widget creatGroupContainer(BuildContext context) {
  double height = MediaQuery.of(context).size.height;
  double width = MediaQuery.of(context).size.width;
  return Container(
    width: width * 0.885,
    height: height * 0.16,
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      gradient: const LinearGradient(
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
        colors: [Color(0xff0059b4), Color(0xff65b2ff)],
      ),
    ),
    child: Stack(
      children: [
        Column(
          children: [
            Padding(
              padding:
                  EdgeInsets.only(top: height * 0.0200, left: width * 0.046),
              child: Row(
                children: [
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(2.5),
                        color: Colors.white),
                    width: width * 0.07,
                    height: height * 0.03,
                    child: const Center(
                      child: FaIcon(
                        Icons.arrow_forward_ios_rounded,
                      ),
                    ),
                  ),
                  SizedBox(width: width * 0.05),
                  const Text(StringRes.cricketRoyalClub,
                      style: TextStyle(
                          fontFamily: 'aplFonts',
                          color: Colors.white,
                          fontSize: 13,
                          fontWeight: FontWeight.w600))
                ],
              ),
            ),
            Padding(
              padding:
                  EdgeInsets.only(right: width * 0.065, top: height * 0.007),
              child: const Text(
                StringRes.containerText,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 10.7,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: width * 0.04),
              child: Row(
                children: [
                  const Text(
                    StringRes.containerText2,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 11,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: height * 0.005),
                    child: InkWell(
                      onTap: () {},
                      child: Container(
                        height: height * 0.020,
                        width: width * 0.13,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(4),
                        ),
                        child: Center(
                          child: Text(
                            StringRes.more,
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                color: Colors.blue.shade600),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(right: width * 0.435),
              child: Text(StringRes.dash,
                  style: TextStyle(color: Colors.grey.shade700)),
            ),
            Padding(
              padding: EdgeInsets.only(left: width * 0.04),
              child: const Row(
                children: [
                  Text(StringRes.joinCode,
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.w500)),
                  Text(StringRes.text27ff9864,
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.w800)),
                  FaIcon(
                    Icons.copy,
                    size: 15,
                    color: Colors.white,
                  )
                ],
              ),
            )
          ],
        ),
        Positioned(
          top: height * 0.11,
          left: width * 0.60,
          child: InkWell(
            onTap: () {},
            child: Container(
              width: width * 0.25,
              height: height * 0.037,
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.white, width: 2.5),
                  borderRadius: BorderRadius.circular(8)),
              child: const Center(
                child: Text(StringRes.viewGroup,
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                        color: Colors.white)),
              ),
            ),
          ),
        ),
      ],
    ),
  );
}

///______________________________________________________________________ edit Screen container ______________________________________________________________///
Widget editScreenCommonContainer(double height, double width) {
  return Container(
    height: height,
    width: width,
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      gradient: const LinearGradient(
          colors: [Color(0xff0059b4), Color(0xff65b2ff)],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter),
    ),
    child: Stack(
      children: [
        Positioned(
          bottom: 0,
          right: 0,
          child: SizedBox(
            height: height * 0.55,
            width: width * 0.55,
            child: Image.asset(
              ImageRes.lion,
              fit: BoxFit.fill,
            ),
          ),
        ),
        Column(
          children: [
            Padding(
              padding: EdgeInsets.only(top: height * 0.21, left: width * 0.086),
              child: Row(
                children: [
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(2.5),
                        color: Colors.white),
                    width: width * 0.065,
                    height: height * 0.06,
                    child: const Center(
                      child: FaIcon(
                        Icons.arrow_forward_ios_rounded,
                      ),
                    ),
                  ),
                  SizedBox(width: width * 0.05),
                  const Text(StringRes.cricketRoyalClub,
                      style: TextStyle(
                          fontFamily: 'aplFonts',
                          color: Colors.white,
                          fontSize: 14,
                          fontWeight: FontWeight.w600))
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: width * 0.09, top: height * 0.01),
              child: const Text(StringRes.royalClubText,
                  style: TextStyle(
                    fontSize: 10,
                    color: Colors.white,
                    fontWeight: FontWeight.w300,
                  )),
            ),
            SizedBox(height: height * 0.02),
            const Divider(
                thickness: 2, endIndent: 45, indent: 45, color: Colors.white70),
            Padding(
              padding: EdgeInsets.only(top: height * 0.01, left: width * 0.086),
              child: Row(
                children: [
                  const Center(
                    child: FaIcon(
                      Icons.arrow_forward_ios_rounded,
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(width: width * 0.035),
                  const Text(
                    StringRes.groupMember,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 10.5,
                        fontWeight: FontWeight.w500),
                  ),
                ],
              ),
            ),
            SizedBox(height: height * 0.11),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                SizedBox(width: width * 0.07),
                container1by2(),
                const SizedBox(width: 4),
                container1by2(),
                const SizedBox(width: 4),
                container1by2(),
                const SizedBox(width: 4),
                container1by2(),
                SizedBox(width: width * 0.07),
              ],
            ),
            SizedBox(height: width * 0.035),
            ElevatedButton(
              style: const ButtonStyle(
                backgroundColor: MaterialStatePropertyAll(Colors.white),
              ),
              onPressed: () {},
              child: Container(
                width: 85,
                height: 25,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                ),
                child: const Center(
                    child: Text(StringRes.creat,
                        style: TextStyle(color: Colors.blue, fontSize: 17))),
              ),
            ),
          ],
        ),
        Padding(
          padding: EdgeInsets.only(top: height * 0.59, left: width * 0.101),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              CircleAvatar(
                  radius: 15,
                  backgroundColor: Colors.white,
                  child: Image.asset(
                    ImageRes.circleAvtar2,
                  )),
              SizedBox(width: width * 0.001),
              CircleAvatar(
                  radius: 15,
                  backgroundColor: Colors.white,
                  child: Image.asset(
                    ImageRes.circleAvtar2,
                  )),
              SizedBox(width: width * 0.001),
              CircleAvatar(
                  radius: 15,
                  backgroundColor: Colors.white,
                  child: Image.asset(
                    ImageRes.circleAvtar2,
                  )),
              SizedBox(width: width * 0.01),
              CircleAvatar(
                radius: 15,
                backgroundColor: Colors.white,
                child: Image.asset(
                  ImageRes.circleAvtar2,
                ),
              ),
              SizedBox(width: width * 0.04),
            ],
          ),
        ),
      ],
    ),
  );
}
////////////////////////////////////////////////////////////////////    container 1/2   /////////////////////////////////////////////////////////////////////////

Widget container1by2() {
  return Container(
    width: 65,
    height: 90,
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(5),
      color: Colors.white,
    ),
    child: const Column(
      children: [
        SizedBox(height: 20),
        Text(StringRes.avniyK,
            style: TextStyle(
              fontSize: 8,
              fontWeight: FontWeight.w600,
            )),
        Divider(indent: 11, endIndent: 11, color: Colors.blue, thickness: 1),
        Text(StringRes.joinDate,
            style: TextStyle(
              fontSize: 8,
              fontWeight: FontWeight.w500,
            ))
      ],
    ),
  );
}

///[Color(0xff0059b4), Color(0xff65b2ff)]
